



// Simpler function:
// function firstNonRepeatedCharacter(string) {
//   for (var i = 0; i < string.length; i++) {
//     var c = string.charAt(i);
//     if (string.indexOf(c) == i && string.indexOf(c, i + 1) == -1) {
//       return c;
//     }
//   }
//   return null;
// }
//
// var someString = 'aabcbd';
// console.log(firstNonRepeatedCharacter(someString));
//
//
//
//  OR


module.exports = function (string) {
  var checkChar,
      prevCharacter;

  checkChar = (function () {
    var repeated = false;

    return function (char) {
      if (prevCharacter && char === prevCharacter) {
        repeated = true;
      }
      if (prevCharacter && char !== prevCharacter) {
        if (!repeated) { return true; }
        repeated = false;
      }
      prevCharacter = char;
      // Return false to say it's not been repeated
      return false;
    };
  })();

  // Interate one extra time past the last character
  for (var i = 0; i <= string.length; i++) {
    if (checkChar(string[i])) { return prevCharacter; }
  }
};
