// Accepts a number and an array of multiples
var f = function sumOfMultiples (number, multiples) {
  return Array.apply(null, new Array(number)).map(function (_, index) {
    return index;
  }).filter(function (number) {
    return multiples.some(function (multiple) {
      return number % multiple === 0;
    });
  }).reduce(function (memo, number) {
    console.log(number)
    return memo + number;
  });
};
console.log(f(10,[3,5]))
