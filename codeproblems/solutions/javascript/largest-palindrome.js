function isPalindrome (str) {
  const mid = Math.floor(str.length / 2)

  for (let i = 0; i < mid; i++) {
    if (str[i] !== str[str.length - 1 - i]) {
      return false
    }
  }

  return true
}

var f = function longestPalindrome (str) {
  let longest = ''

  for (let i = 0; i < str.length; i++) {
    let len = str.length
    while (len > i && len - i > longest.length) {
      console.log("i, len->",i,len," largest->",longest," len-i->",len-i);
      const substring = str.substring(i, len--)

      if (isPalindrome(substring)) {
        longest = substring
      }
    }
  }

  return longest
}

console.log(f("I am a red racecar driver"))
