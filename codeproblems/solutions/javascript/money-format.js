var f = function (value) {
  var remaining = value - ~~value,
      string    = '' + ~~value,
      length    = string.length,
      places    = 0;

  while (--length) {
    console.log(length,places)
    places += 1;
    // At every third position we want to insert a comma
    if (places % 3 === 0) {
      string = string.substr(0, length) + ',' + string.substr(length);
    }
  }
  console.log(remaining.toFixed(2).slice(0),remaining.toFixed(2).slice(1))
  return '$' + string + remaining.toFixed(2).slice(1);
};
console.log(f(2310000.159897));
