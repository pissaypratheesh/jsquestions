/**
 * Created by m01352 on 22/03/17.
 */
All in one underscore.js: http://underscorejs.org/docs/underscore.html




    Supperb short awesome Qs: https://github.com/apoterenko/javascript-interview-questions

    es6 quiz: http://perfectionkills.com/javascript-quiz-es6/


    Curry function..and function composition.
like f(g(x))

const curry = fn => (...args) => fn.bind(null, ...args);

const map = curry((fn, arr) => arr.map(fn));

const join = curry((str, arr) => arr.join(str));

const toLowerCase = str => str.toLowerCase();

const split = curry((splitOn, str) => str.split(splitOn));
const toSlug = input => encodeURIComponent(
    join('-')(
        map(toLowerCase)(
            split(' ')(
                input
            )
        )
    )
);
OR
const compose = (...fns) => x => fns.reduceRight((v, f) => f(v), x);
const toSlug = compose(
    encodeURIComponent,
    join('-'),
    map(toLowerCase),
    split(' ')
);

console.log(toSlug('JS Cheerleader')); // 'js-cheerleader'


“use strict” what all it does: http://web.archive.org/web/20141031154636/http://www.webdesignporto.com/why-use-strict-in-javascript-can-save-you-hours/
    Benefits:
        Duplicate keys in object.
    Variables without var
    Duplicate arguments
Freezes the arguments of the functions
-------------------------

    All frequently asked Qs: https://quizlet.com/72050035/interview-js-flash-cards/

    var a = (2,3,5) whats a?
    5, comma operator evaluates each of it's operands (left to right) and returns value of it's last operand

------------------------------

var f = (function f(){ return "1"; }, function g(){ return 2; })();
console.log(
    f, //2
    typeof f //number
)


------------------
    Question: What is the value of foo.x?

var foo = {n: 1};
var bar = foo;
foo.x = foo = {n: 2};
nice and imp: object internal linking pratheesh favourite  question
http://stackoverflow.com/questions/30158304/two-assignments-in-one-line
    http://stackoverflow.com/questions/34933210/why-is-the-value-of-foo-x-undefined-in-foo-x-foo-n-2
        -----------------------




            function foo(x, y, ...z) {
                console.log( x, y, z );
            }

foo( 1, 2, 3, 4, 5 );           // 1 2 [3,4,5]


function foo(...args) {
    console.log( args );
}

foo( 1, 2, 3, 4, 5);            // [1,2,3,4,5]


---------------------
var my_object = {
    '0': 'zero',
    '1': 'one',
    '2': 'two',
    '3': 'three',
    '4': 'four',
    length: 5
};

var sliced = Array.prototype.slice.call( my_object, 3 ); //['three','four'];
Very useful for getting function arguments into array

arguments object as the this value of .slice(). Because arguments has a .length property and a bunch of numeric indices, .slice() just goes about its work as if it were working on a real Array.
------------------------


var w = 1, z = 2;

function foo( x = w + 1, y = x + 1, z = z + 1 ) {
    console.log( x, y, z );
}

foo();                  // ReferenceError
The w in the w + 1 default value expression looks for w in the formal parameters' scope, but does not find it, so the outer scope's w is used. Next, The x in the x + 1 default value expression finds x in the formal parameters' scope, and luckily x has already been initialized, so the assignment to y works fine.

However, the z in z + 1 finds z as a not-yet-initialized-at-that-moment parameter variable, so it never tries to find the z from the outer scope.


    As we mentioned in the "let Declarations" section earlier in this chapter, ES6 has a TDZ, which prevents a variable from being accessed in its uninitialized state. As such, the z + 1 default value expression throws a TDZ ReferenceErrorerror.


------------------------

    Simple and easy swapping in es6

var x = 10, y = 20;

[ y, x ] = [ x, y ];

console.log( x, y );                // 20 10
--------------------------------------
var o = { a:1, b:2, c:3 },
    p = [4,5,6],
    a, b, c, x, y, z;

( {a} = {b,c} = o );
[x,y] = [z] = p;

console.log( a, b, c );         // 1 2 3
console.log( x, y, z );         // 4 5 4
--------------------
    Question: What does the following code print?
    console.log('one');
setTimeout(function() {
    console.log('two');
}, 0);
console.log('three');

------------------------

var a = ["a","b","c","d","e"];

for (var idx in a) {
    console.log( idx );
}
// 0 1 2 3 4

for (var val of a) {
    console.log( val );
}
// "a" "b" "c" "d" "e"

As you can see, for..in loops over the keys/indexes in the a array, while for..of loops over the values in a.

----------------------------

    'use strict';

let sum = (arr) => arr.reduce((a, b) => a + b);
let addGenerator = (numArgs, prevArgs) => {
    return function () {
        let totalArgs = prevArgs.concat(Array.from(arguments));
        if (totalArgs.length === numArgs) {
            return sum(totalArgs);
        }
        return addGenerator(numArgs, totalArgs);
    };
};

let add = addGenerator(2, []);

add(2, 5); // 7
add(2)(5); // 7
add()(2, 5); // 7
add()(2)(5); // 7
add()()(2)(5); // 7

OR

var sumAnyway = function(func){
    var totalArgs = func.length;
    return function recurAdd(){
        var args = Array.from(arguments);

        if(args.length === totalArgs){
            return func.apply(null,args);
        }
        return function(){
            var args2 = Array.from(arguments);
            return recurAdd.apply(null,args.concat(args2))
        }
    }
}

var wow = sumAnyway(function(a,b,C){
    return Array.from(arguments).reduce((a,b)=> a+b);
});

console.log(wow(2)(3)(4))

---------------------------

var k = [1,1,23]
k.length = 1
print k//[1]
k.length=0
print k//[]
-------------------------


    What will be the output of the following code?

var output = (function(x) {
    delete x;
    return x;
})(0);

console.log(output);
Above code will output 0 as output. delete operator is used to delete a property from an object. Here x is not an object it's local variable. delete operator doesn't affect local variable.




----------------------
    copy an array:
    var old_array = [1, 2, 3];

// This works
var new_array_1 = old_array.slice();

// This works too
var new_array_2 = [].concat(old_array);

// This does NOT work, because any edits to new_array_3
// will alter the original, old_array. NOT what we want!
var new_array_3 = old_array;

splice will mutate the original array but slice will not


-----------------------


    (function test () {
        console.log(
            {}.constructor === arguments.constructor,
            [].constructor === arguments.constructor
        );
    })();
VM6299:2 //true false

-----------------------------

    (function test() {
        console.log(
            function () {} instanceof Object,
            function () {} instanceof Function,
            Function instanceof Object,
            Object instanceof Function
        );
    })();

ANS: all true
----------
    (function test() {
        console.log(
            function () {}.apply.length, //2
            Function.prototype.apply.length //2
        );
    })();

--------------
    What will be the output of the following code?


var Employee = {
    company: 'xyz'
}
var emp1 = Object.create(Employee);
delete emp1.company
console.log(emp1.company);
Above code will output xyz as output. Here emp1 object got company as prototype property. delete operator doesn't delete prototype property.


emp1 object doesn't have company as its own property. you can test it console.log(emp1.hasOwnProperty('company')); //output : false However, we can delete company property directly from Employee object using delete Employee.company or we can also delete from emp1 object using __proto__property delete emp1.__proto__.company.


--------------------------

    What will be the output of the following code?


var bar = true;
console.log(bar + 0);
console.log(bar + "xyz");
console.log(bar + true);
console.log(bar + false);

all below are true:
Function === Object.constructor,
Function === Number.constructor,
Function === Function.constructor,
Function === Window.constructor,
Object === Object.prototype.constructor,
Number === Number.prototype.constructor,
Array === Array.prototype.constructor,
Window === Window.prototype.constructor
Above code will output 1, "truexyz", 2, 1 as output. General guideline for addition of operator:

Number + Number -> Addition
Boolean + Number -> Addition
Number + String -> Concatenation
String + Boolean -> Concatenation
String + String -> Concatenation

------------------------------

var z = 1; var y = z = typeof y;
console.log(y,z); //string string in chrome and undef undef in firefox

--------------------

    function foo() {
        return foo;
    }
new foo() instanceof foo; //false


Here function foo is returning foo which is again pointer to function foo


-------------------------


    What would be the output of the following code?


    function User(name) {
        this.name = name || "JsGeeks";
    }

var person = new User("xyz")["location"] = "USA";
console.log(person);
The output of above code would be USA. Here new User("xyz") creates a brand new object and created property location on that and USA has been assigned to object property location and that has been referenced by the person.


    Let say new User("xyz") crated a object called foo and returned now foo["location"] would be assigned value as USA and person is referencing to foo["location"].

----------------------------


    Write a function called deepClone which takes an object and creates a object copy of it.


    _.deepExtend
var newObject = deepClone(obj);
Solution:

    function deepClone(object){
        var newObject = {};
        for(var key in object){
            if(typeof object[key] === 'object'){
                newObject[key] = deepClone(object[key]);
            }else{
                newObject[key] = object[key];
            }
        }
        return newObject;


        ----------------------------------

            (function () {
                var array = new Array('a', 'b', 'c', 'd', 'e');
                array[10] = 'f';
                delete array[10];
                console.log(array.length); //11
            }());

        ------------------------------
            console.log(4+3+2+"1")?
            91

            console.log(typeof NaN) //number
        console.log(2+true) //3
        console.log(-'34'+10) //-24
        var a = (2, 3, 5); console.log(a) //5
        console.log(3 instanceof Number) //false
        false, because 3 is a primitive. console.log(new Number(4) instanceof Number) will return true.

        console.log(typeof null) //object

        '1' + 2 +  3 ; // Equals '123'
        3  + 2 + '1'; // Equals '51'
        3  + 2 +  1 ; // Equals 6
        ---------------

        var obj = {
            message: "Hello",
            innerMessage: !(function() {
                console.log(this.message + " inner");//undefined inner
            })(),
            k: function(){
                console.log(this.message+" of k"); //Hello of k
                return "fucnker"
            }
        };

        console.log(obj.k());//fucnker

        -------------------------------

        var permutate = function(str){
            var res = {};
            (function makeWord(word,remaining){
                if(!remaining){
                    return res[word] = true;
                }
                for(var i =0;i<remaining.length;i++){
                    makeWord(
                        word + remaining[i],
                        remaining.substr(0,i) + remaining.substr(i+1)
                    )
                }
            })("",str)
            return Object.keys(res);
        }

        console.log(permutate("abc"))
        --------------------------


            (function test() {
                var a = 1;
                function test() {
                    if (!a) {
                        var a = 10;
                    }
                    console.log(a); //10
                }
                test();
                console.log(a); //1
            })();
        ----------------------------


            (function test() {
                (function () {
                    var a = b = 3;
                })();

                console.log(
                    typeof a,  //undefined
                    typeof b   //number
                );
            })();

        ---------------------

        var u = [1,2,[3,4]] + []
“1,2,3,4"

            [1,2,[3,4]] + [[5,6], 7, 8]
        "1,2,3,45,6,7,8"

        -------------------

            console.log(
                9 < 5 < 1,   // (9<5) is false i.e. 0, 0<1 so true is answer
                2 < 6 < 1,   //false as 1<1
                1 < 3 < 4    //true as true i.e. 1<4
            );

        ------------------------
            (function test() {
                console.log(
                    [] - [], //0
                    [] + [], //“"
                    {} - {}, //NaN
                    {} + {} //"[object Object][object Object]"
                );
            })();

        ------------------------------

            (function test() {
                function sum(a, b) {
                    return a + b;
                }

                function sum(c) {
                    return c;
                }

                console.log(sum(3));   //3 function overwrite
                console.log(sum(2, 4)); //2 function overwrite
            })();
        -------------------------------


            (function test () {
                "use strict";

                var holder, fn;
                holder = {
                    holderFn: function () {
                        console.log("this->",this);
                    }
                };
                fn = holder.holderFn;

                holder.holderFn(); //prints this as the holderFn object
                fn(); //prints this as undefined cz of use strict line,, global vars not allowed
            })();
        ----------------------------

            (function test() {
                function fn() {
                    arguments.callee.count = arguments.callee.count || 0;
                    return arguments.callee.count++;
                }

                console.log(
                    fn(), //0
                    fn(), //1
                    fn() //2
                );
            })();

        -----------------------

            (function test() {
                var a = '5', b = 2, c = a+++b; // (a++)+b
                console.log(a,b,c); //6,2,7
            })();
        ------------------------

            (function () {
                var fn = function () {
                    console.log(typeof this,this);
                };
                fn.call("Hello World!"); })();

        ANS:
            //object, “Hello world"


            (function () {
                "use strict";
                var fn = function () {
                    console.log(typeof this,this);
                };
                fn.call("Hello World!");
            })();

//String , “Hello world"



        --------------------------
            =----------------------------

                fill in the factorial into an array:
            (function test() {
//"use strict” // if we use this line then arguments.callee will throw error
                console.log(
                    [1, 2, 3, 4, 5].map(function (n) {
                        return n === 1 && 1 || arguments.callee(n - 1) * n;
                    })
                );
            })();

        Same above in strict mode—>
        (function test() {
            "use strict";
            console.log(
                [1, 2, 3, 4, 5].map(function work(n) {
                    return n === 1 && 1 || work(n - 1) * n;
                })
            );
        })();
        ----------------------------

        var s1 = “eeeee"
        s1[2] = 'w';
        console.log(s1); //“eeeee"



        -----------------------
            (function () {
                var a = 1;
                (new Function('a = 2'))();
                console.log('a1:', a) //a1: 1
            })();

        console.log('a2:', a)//a2: 2

        ----------------------
            (function test() {
                var x, y, z;
                x = (y = 1, z = 2);

                console.log(x); //Last one will get assigned
            })();
        -------------------

            Number.prototype.multiply = function(a){
            return cation(this,a)
        }

        function cation(a,b){
            return a*b;
        }
        console.log((14).multiply(6))
        -------------------

            (function () {
                console.log(
                    new Function('return this')()
                );
                console.log(
                    new Function('"use strict"; return this')()
                );
            })();
        ------------------
            (function test() {
                var a = {},
                    b = {value: 'test1'},
                    c = {value: 'test2'};

                a[b] = 'test3';
                a[c] = 'test4';
                a[b] = "prath"

                console.log(a[b],Object.keys(a)); //"prath", ["[object Object]"]
            })();
        -------------------------

            function f(a){
                a = 42;
                return [a, arguments[0]];
            }
        var pair = f(17);
        console.log(pair);
        VM162:7 [42, 42]
        ----------------
            function f(a){
                "use strict";
                a = 42;
                return [a, arguments[0]];
            }
        var pair = f(17);
        console.log(pair);
        VM175:7 [42, 17]

        ---------------------

            "use strict";
        var f = function() { return arguments.callee; };
        f(); // throws a TypeError with “use strict”…..else will print function without “use strict"
        --------------------------
            1.
        "use strict";
        function abcd(){
            pid = 12;
        }
        let pid = null;
        abcd();
        console.log(pid) //12

        2.
        const MARKUP;
        console.log(MARKUP) //syntax error, cz const must be initialised always

        3.
        const AAA  = 100;
        if(AAA > 0){
            const AAA = 10;
        }
        console.log(AAA) //100, cz of block scoping in es6

        4. Don’t need to use parenthesis around arguments passed if there is one input to arrow function

        5. var abc = {
            num: 123,
            process: () => console.log(this); //window, cz of arrow fn
    };
        invoice.process();

        AND

        var abc = {
            num: 123,
            process: function(){
                return () => console.log(this.num); //123 cz its inside function
            }
        };
        invoice.process()();




        https://www.youtube.com/watch?v=FAZJsxcykPs&list=UUGGRRqAjPm6sL3-WGBDnKJA
            https://www.youtube.com/watch?v=z-yU0sVdKTo&list=UUGGRRqAjPm6sL3-WGBDnKJA
                https://www.youtube.com/watch?v=O1YP8QP9gLA


                    https://github.com/kennymkchan/interview-questions-in-javascript
                        http://perfectionkills.com/understanding-delete/
                            https://github.com/vvscode/js--interview-questions
                                https://github.com/kennymkchan/interview-questions-in-javascript

                                    y nodejs? https://www.quora.com/What-are-the-advantages-of-using-Node-js
            https://dzone.com/articles/what-are-benefits-nodejs

        MUST read: http://www.lazyquestion.com/interview-questions-and-answer/nodejs

            Resume: https://www.visualcv.com/app/#/cvs/1895195
                MASTER Link for js question:https://github.com/vvscode/js--interview-questions

            HUGE promise implementation:https://www.promisejs.org/implementing/

            MASTER book for es6: https://leanpub.com/understandinges6/read  by Nicholas C. Zakas  Understanding ECMAScript 6

            https://github.com/MaximAbramchuck/awesome-interview-questions

                MUST READ:
            http://davidbcalhoun.com/2011/different-ways-of-defining-functions-in-javascript-this-is-madness/

                http://eloquentjavascript.net/code/#3.2

                    Suggested by Anshu must read:
            https://medium.com/javascript-scene/search?q=Master%20the%20JavaScript%20Interview
                https://medium.com/javascript-scene/tagged/learnjs
                    https://medium.com/javascript-scene/master-the-javascript-interview-what-s-the-difference-between-class-prototypal-inheritance-e4cd0a7562e9#.n2o3ulnao


                        http://javascript.crockford.com/little.html  (read about Y combinator, recursion related)
                            https://medium.com/javascript-scene/10-interview-questions-every-javascript-developer-should-know-6fa6bdf5ad95#.2nuyg7bs5
                                https://github.com/getify/You-Dont-Know-JS
                                    http://chimera.labs.oreilly.com/books/1234000000262/index.html

                                        http://chimera.labs.oreilly.com/books/1234000000262/index.html
                                            https://medium.com/javascript-scene/10-interview-questions-every-javascript-developer-should-know-6fa6bdf5ad95#.2flj1zehb


                                                JS fiddle must read quick glance:
            http://jsfiddle.net/javascriptenlightenment/2QLvv/
                http://jsfiddle.net/javascriptenlightenment/fQHJB/ (very nice)


                    Go thru for exercises:
        https://www.quora.com/I-need-JavaScript-practice-with-problems-and-solutions-is-there-a-good-site (tells wr to learn from)
            https://www.codeschool.com/courses/javascript-road-trip-part-3
                https://www.freecodecamp.com/challenges/learn-how-free-code-camp-works
                    https://coderbyte.com/editor/guest:First%20Factorial:JavaScript


                        Queue implementation in JS:
        var Q=new Array();
        Q.unshift("A);
        Q.unshift("B");
        Q.unshift("C");


        alert(Q.pop());
        alert(Q.pop());
        alert(Q.pop());

        DEBOUNCE function implementation: https://davidwalsh.name/javascript-debounce-function


        MUST READ: https://github.com/aharris88/solutions-javascript-the-good-parts-exersises
            1.function funcky(o){
            o=null;
        }
        var x=[ ];
        funcky(x);
        console.log("x-->",x);
        ANS: x--> [ ]
        But it can modify the children of that obj sent

        2. Write a function that takes a binary function and makes it callable with two invocations:
            like
        addf = applyf(add)
        addf(3)(4)  //7
        applyf(mul)(5)(6)
        ANS:
            function applyf(binary){
                return function(x){
                    return function(y){
                        return binary(x,y);
                    }
                }
            }

        3.Write a function that takes  a function and an argument and returns a function that can supply a second arguement
        like
        add3 = curry(add,3)
        add3(4) //7
        curry(mul,3)(4) //12

        ANS:
            var curry = function(binary, fir){
                return function(sec){
                    return binary(fir,sec);
                }
            }
        var mul = function(a,b){
            return a*b;
        }
        console.log(curry(mul,2)(3))

        4. without writing any new function show 3ways to create inc function
        ANS:
        1) inc = addf(1)
        2) inc= applyf(add) (1);
3) inc = curry(add , 1);

5. Write methodize, a function that converts a binary function to a method.
    like Number.prototype.add = methodize(add);
(3).add(4) //7
ANS:
    function methodize (func){
        return function(…y){
            return func(this, …y);
        }
    }

ES5:
    Number.prototype.add = methodize(add);
function methodize(func) {
    return function(y){
        return func(this,y);
    }
}
function add(a,b){
    return a+b;
}
console.log((2).add(3))

6. Write demethodize, a function that converts a method to a binary function.
demethodize(Number.prototype.add) (5,6) //11
ANS: DOesnt work tho
function add(x,y){
    return x + y;
}
//add(1,2) => 3

function methodize(fn){
    return function(x){
        return fn(this, x);
    };
};
Number.prototype.Add = methodize(add);
//(1).Add(2) => 3

function demethodize (fn){
    return function(x,y){
        return fn.call(x,y);
    };
}
var newAdd = demethodize(Number.prototype.Add);
// newAdd(1,2) => 3


7. Write twice function:
var double = twice(add);
double(11) //22
var square = twice(mul);
square(11) //121
ANS:
    function twice(func) {
        return function(y){
            return func(y,y);
        }
    }
function add(a,b){
    return a+b;
}
var double = twice(add);
console.log(double(11))
const twice = fn => x => fn(x,x);

8. Write composeu function:
composeu(double, square)(3) //36
ANS:
    function composeu(f,g) {
        return function(y){
            return g(f(y));
        }
    }
function square(a){
    return a*a;
}
var double = function(x){
    return x*2;
};
console.log(composeu(double,square)(3))
//in es6 const composeu = (f,g) => y => g(f(y))
9.write composeb function:
function composeb(f,g) {
    return function(x,y,z){
        return g(f(x,y),z);
    }
}
function add(a,b) {
    return a+b;
}
function mul(a,b) {
    return a*b;
}
console.log(composeb(add,mul)(2,3,5)) to print (2+3)*5 = 25

10. Write a function that allows another function to be called only once:
    add_once = once(add);
add_once(3,4) //7
add_once(3,4) //throw!
ANS:
    function add(a,b) {
        return a+b;
    }
function once(func){
    return function(){
        var f = func;
        func = null;
        return f.apply(this,arguments);
    };
}
add_once = once(add);
console.log(add_once(3,4) );//7
console.log(add_once(3,4) );//throw!

11.Write a factory function that returns two functions that implements an up/down counter
ANS:

    function counterf(val){
        return {
            inc: function(){
                val = val + 1;
                return val;
            },

            dec: function(){

                val = val - 1;
                return val;
            }
        }
    }
var counter = counterf(11);
console.log(counter.inc());
console.log(counter.dec());

