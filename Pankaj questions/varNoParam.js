
var sumAnyway = function(func){
  var totalArgs = func.length;
  return function recurAdd(){
    var args = Array.from(arguments);

    if(args.length === totalArgs){
      return func.apply(null,args);
    }
    return function(){
      var args2 = Array.from(arguments);
      return recurAdd.apply(null,args.concat(args2))
    }
  }
}

var wow = sumAnyway(function(a,b,c){
  console.log(a,b,c,arguments)
  return Array.from(arguments).reduce((a,b)=> a+b);
});

console.log(wow(2)(3)(4))
//console.log(wow(2)(3)(4)(5))