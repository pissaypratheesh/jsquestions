var permutate = function(str){
  var res = {};
  (function makeWord(word,remaining){
    if(!remaining){
      return res[word] = true;
    }
    for(var i =0;i<remaining.length;i++){
      makeWord(        word + remaining[i],        remaining.substr(0,i) + remaining.substr(i+1)      )
    }
  })("",str)
  return Object.keys(res);
}

console.log(permutate("abc"))
