


// Next code print '0,1,2,3,4,4,3,2,1,0'
// Modify only functions ( not loops ) to get output as '4,3,2,1,0,0,1,2,3,4'
var a = function(i) { console.log(i); };
var b = function(i) { console.log(i); };
for(let i = 0; i < 5; i++) {  a(i); }
for(let i = 4; i>=0; i--) { b(i); }
