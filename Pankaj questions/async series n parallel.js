var async = {
  serial:   function(fnArray, callback){
    var err=[], res=[];
    (function asyncSerial(fnArray, callback, err, res){
      if (fnArray.length) {
         var fn = fnArray.shift();
         fn(function (er,rs) {
           err.push(er);
           res.push(rs)
           asyncSerial(fnArray,callback, err,res);
         });
     }
     else {
         callback(err,res);
     }
    })(fnArray, callback,err,res);
  },
  
  
   
  parallel: function(fnArray, callback){
    var err=[], res=[];
    var totalFn = fnArray.length;
    function finish(){
      totalFn-=1;
      if(totalFn === 0){
        callback(err,res)
      }
    }
    (function asyncParallel(fnArray, callback, err, res){
      for(let fn in fnArray){
        fnArray[fn](function (er,rs) {
           err.push(er);
           res.push(rs);
           finish();
         });
      }
    })(fnArray, callback,err,res);
  } 
}




var first = function(callback){
    console.log("starting first");
  setTimeout(function(){ 
    console.log("finished first");
    callback(null,"success first");
}, 1000)  

}


var sec = function(callback){
    console.log("starting sec");

  setTimeout(function(){ 
    console.log("finished second");
    callback(null,"success second");
}, 2000)  
}


var third = function(callback){
  console.log("starting third");
  setTimeout(function(){ 
    console.log("finished third");
    callback(null,"success second");
}, 3000)  
}


async.parallel([first,sec,third],function(err,res){
  console.log("done")
  console.log("err n res-->",err,res)
})

async.serial([first,sec],function(err,res){
  console.log("done")
  console.log(err,res)
})